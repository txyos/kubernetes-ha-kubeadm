# 五.Ubuntu 20.04 部署K8S

> 使用kubeadm来安装, 要求所有的主机节点都需要安装docker，具体的docker安装请参考
>
> https://docs.docker.com/engine/install/ubuntu/

## 1.前提条件

```bash
# 关闭swap
$ sudo swapoff -a
# 注释掉 /etc/fstab 这个文件中包含swap的那行
$ sudo sed -i '/ swap / s/^/#/' /etc/fstab
# 修改时区
$ sudo timedatectl set-timezone Asia/Shanghai

# 为root用户设置密码
# 1、先用安装时候的用户登录进入系统
# 2、输入：sudo passwd  按回车
# 3、输入新密码，重复输入密码，最后提示passwd：password updated sucessfully 此时已完成root密码的设置
# 4、输入：su root 进行登录

# 开启时间同步，非必须
$ sudo echo "*/3 * * * * /usr/sbin/ntpdate ntp3.aliyun.com &> /dev/null" > /tmp/crontab
$ crontab /tmp/crontab
```

## 2.配置系统参数

```bash
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables=1
net.bridge.bridge-nf-call-iptables=1
net.ipv4.ip_forward=1
vm.swappiness=0
vm.overcommit_memory=1
vm.panic_on_oom=0
fs.inotify.max_user_watches=89100
EOF
# 生效
sudo sysctl --system

# 以下为优化项说明，供参阅
# 优化内核参数详解
cat > /etc/sysctl.d/kubernetes.conf <<EOF
net.bridge.bridge-nf-call-iptables=1
net.bridge.bridge-nf-call-ip6tables=1
net.ipv4.ip_forward=1
#由于tcp_tw_recycle与kubernetes的NAT冲突，必须关闭！否则会导致服务不通。4.1x内核已经废弃这项了
#net.ipv4.tcp_tw_recycle=0
#禁止使用 swap 空间，只有当系统 OOM 时才允许使用它
vm.swappiness=0
#不检查物理内存是否够用
vm.overcommit_memory=1
#开启 OOM
vm.panic_on_oom=0
fs.inotify.max_user_instances=8192
fs.inotify.max_user_watches=1048576
fs.file-max=52706963
fs.nr_open=52706963
#关闭不使用的ipv6协议栈，防止触发docker BUG.
net.ipv6.conf.all.disable_ipv6=1
net.netfilter.nf_conntrack_max=2310720
EOF
```

## 3.配置阿里云源地址

```bash
# 添加阿里云的k8s源
$ sudo sh -c 'echo "deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list'
$ wget --quiet -O - https://mirrors.aliyun.com/kubernetes/apt/doc/apt-key.gpg | sudo apt-key add -

$ sudo apt-get update
# 查看可安装版本
$ sudo apt-cache madison kubelet
# 安装指定版本
# kubectl组件仅在控制节点上安装即可 
$ sudo apt-get install -y kubelet=1.19.7-00 kubeadm=1.19.7-00 kubectl=1.19.7-00
# 安装最新版本命令 sudo apt-get install -y kubelet kubeadm kubectl

# 启动kubelet 所有的node节点
$ sudo systemctl enable kubelet && sudo systemctl start kubelet

# 以下命令可以查看kubeadm 需要的docker镜像列表
# kubeadm config images list [--kubernetes-version <version>]
# 查看 v1.19.7 版本需要的镜像列表，如果docker镜像无法下载的哈，可以查看列表内容，进行手工下载
$ kubeadm config images list --kubernetes-version v1.19.7
```

## 4.进行安装

```bash
# 使用 kubeadm开始安装，在主节点上运行以下命令
# --image-repository 也可以指定自己的Harbor地址
$ sudo kubeadm init \
--apiserver-advertise-address=192.168.200.11 \
--image-repository registry.aliyuncs.com/google_containers \
--kubernetes-version v1.19.7 \
--service-cidr=10.2.0.0/16 \
--pod-network-cidr=172.22.0.0/16

# 运行完成命令后，按照提示进行子节点的安装即可
# 配置好 kubectl 命令后，安装网络组件，详细的过程请仓考 2-ha-deploy.md 这篇文章
$ kubectl apply -f /etc/kubernetes/addons/calico.yaml
```

